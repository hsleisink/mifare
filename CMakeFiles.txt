# mfoc
set(mfoc_src
	src/crapto1.c
	src/crapto1.h
	src/crypto1.c
	src/mfoc.c
	src/mfoc.h
	src/mifare.c
	src/mifare.h
	src/nfc-utils.c
	src/nfc-utils.h
	src/slre.c
	src/slre.h
)

set(manual_page
	man/mfoc.1
)

# mfcuk
set(mfcuk_src
	src/crapto1.c
	src/crapto1.h
	src/crypto1.c
	src/mfcuk.c
	src/mfcuk_finger.c
	src/mfcuk_finger.h
	src/mfcuk.h
	src/mfcuk_mifare.c
	src/mfcuk_mifare.h
	src/mfcuk_utils.c
	src/mfcuk_utils.h
	src/mifare.c
	src/mifare.h
	src/nfc-utils.c
	src/nfc-utils.h
	src/xgetopt.c
	src/xgetopt.h
)

set(mfcuk_data
	data/mfcuk_tmpl_oyster.mfd
	data/mfcuk_tmpl_ratb.mfd
	data/mfcuk_tmpl_skgt.mfd
)
