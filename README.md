The Mifare Classic Offline Cracker (MFOC) and Mifare Classic Universal toolKit (MFCUK) tools have been taken from https://github.com/nfc-tools/.

Compile setup via CMake and small fixes by Hugo Leisink <hugo@leisink.net>. Use this tool for scientific research only. Don't use this tool to commit crimes or to bypass security measures.

Use the following commands to compile and install the tools:

- mkdir build
- cd build
- cmake ..
- make
- sudo make install/strip
