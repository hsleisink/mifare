cmake_minimum_required(VERSION 2.6.0)
project(mfoc C)

# Compiler
set(CMAKE_C_FLAGS "-O2 -Wall -Wextra ${CMAKE_C_FLAGS}")
set(CMAKE_BUILD_TYPE "RelWithDebInfo")

# Includes
include(CMakeFiles.txt)
include(CheckIncludeFile)
include(GNUInstallDirs)

# Settings
set(DATA_DIR ${CMAKE_INSTALL_FULL_LOCALSTATEDIR}/share/mifare CACHE STRING "Data directory")

# Compiler directies
check_include_file(sys/endian.h HAVE_SYS_ENDIAN_H)
check_include_file(endian.h HAVE_ENDIAN_H)
check_include_file(byteswap.h HAVE_BYTESWAP_H)
check_include_file(nfc/nfc.h HAVE_NFC_NFC_H)

if (NOT HAVE_NFC_NFC_H)
	message(FATAL_ERROR "NFC library not found. Install the libnfc-dev package.")
	return()
endif()

# Configure files
configure_file(config.h.in config.h)
include_directories(${CMAKE_CURRENT_BINARY_DIR})

# Binaries
add_executable(mfoc ${mfoc_src})
add_executable(mfcuk ${mfcuk_src})
target_link_libraries(mfoc "nfc")
target_link_libraries(mfcuk "nfc")

# Installation
install(TARGETS mfoc mfcuk DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES ${manual_page} DESTINATION ${CMAKE_INSTALL_FULL_MANDIR}/man1)
install(FILES ${mfcuk_data} DESTINATION ${DATA_DIR})
